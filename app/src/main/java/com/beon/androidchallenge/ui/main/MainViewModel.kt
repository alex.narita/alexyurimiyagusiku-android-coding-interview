package com.beon.androidchallenge.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.beon.androidchallenge.data.repository.FactRepository
import com.beon.androidchallenge.domain.model.Fact

class MainViewModel : ViewModel() {

    val currentFact = MutableLiveData<Fact?>(null)
    val loadingLiveData = MutableLiveData<Boolean>()

    fun searchNumberFact(number: String) {
        if (number.isEmpty()) {
            currentFact.postValue(null)
            return
        }

        loadingLiveData.postValue(true)

        FactRepository.getInstance().getFactForNumber(number, object : FactRepository.FactRepositoryCallback<Fact> {
            override fun onResponse(response: Fact) {
                currentFact.postValue(response)
                loadingLiveData.postValue(false)
                // we should check for the local database if this fact is already saved
                // trigger livedata to show this is a liked fact or not.
            }

            override fun onError() {
                loadingLiveData.postValue(false)
            }

        })
    }

    // saveFact(factString: String)
    // FactRepository.save(factString)
}